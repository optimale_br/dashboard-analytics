$(document).ready(function(){
    $('.input_investimento').mask('000.000.000.000.000,00', {reverse: true});
    $('.input_custo_cte').mask('000.000.000.000.000,00', {reverse: true});
    $('.input_juros').mask('##0,00%', {reverse: true});
    $('.input_economia').mask('000.000.000.000.000,00', {reverse: true});
    $('.input_aumento_energia').mask('##0,00%', {reverse: true});
});

function calculaValores(){

    var input_payback_anos = document.getElementById('input_payback_anos');
    var input_vpl = document.getElementById('input_vpl');
    var input_relacaoCustoBeneficio = document.getElementById('input_relacaoCustoBeneficio');

    var val_investimento = parseFloat($('.input_investimento').cleanVal())/100;
    var val_custo_cte = parseFloat($('.input_custo_cte').cleanVal())/100;
    var val_juros = parseFloat($('.input_juros').cleanVal())/100;
    var val_economia = parseFloat($('.input_economia').cleanVal())/100;
    var val_vida_util = parseFloat($('.input_vida_util').val());
    var val_aumento_energia = parseFloat($('.input_aumento_energia').cleanVal())/100;

    input_payback_anos.value = '';
    input_vpl.value = '';
    input_relacaoCustoBeneficio.value = '';

    if(!input_investimento.value){
        alert('Valor de investimento obrigatório');
        return;
    }

    var resultado =
            calcula(val_investimento, 
                    val_custo_cte, 
                    val_juros, 
                    val_economia, 
                    val_vida_util, 
                    val_aumento_energia);


    input_payback_anos.value = resultado.paybackAnos;
    input_vpl.value = resultado.vpl;
    input_relacaoCustoBeneficio.value = resultado.relacaoCustoBeneficio;

    $('.input_vpl').mask('000.000.000.000.000,00', {reverse: true});
    //calcula(investimento, custoCte, juros, economia, vidaUtil, aumentoEnergia);

    /*if(resultado.vpl < 0){
        input_vpl.style.color = "red";
        document.getElementById("label_negativo").style.display = "";
    }else{
        input_vpl.style.color = "black";
        document.getElementById("label_negativo").style.display = "none";
    }*/

    var dados =
            geraDadosGrafico(val_investimento, 
                            val_custo_cte, 
                            val_juros, 
                            val_economia, 
                            val_vida_util, 
                            val_aumento_energia);

    var intervals = [];
    var values = [];
    for( var i = 0; i < dados.length; i++ ){
        intervals.push(dados[i][0]);
        values.push(dados[i][1]);
    }

    Plotly.newPlot("net_present_value", 
        [
            {"type": "scatter", 
            "x": intervals,
            "y": values,
            "marker" : {"color" : "blue"}
            }
        ], 
        {"paper_bgcolor": "#fff", 
            "plot_bgcolor": "#fff", 
            "font": {"color": "#000"}, 
            "xaxis": {"title": "Years"}, 
            "yaxis": {"title": "Value"}, 
            "title": "Net Present Value"},
            {"showLink": false, "linkText": "Export to plot.ly"});                            
}

function calculaFvp(ano, aumentoEnergia, juros){
    var v11 = Math.pow((1 + aumentoEnergia), ano);
    var v12 = Math.pow((1 + juros), ano);
    var v13 = v11 - v12;

    var v14 = (1+aumentoEnergia)-(1+juros);

    var v15 = v13 / v14;

    var v16 = (1/v12);

    return v15 * v16;
}

function calculaFvp2(ano, juros){
    var v21 = Math.pow((1 + juros), ano) - 1;
    var v22 = juros * Math.pow((1 + juros), ano);
    return v21 / v22;
}

function calculaVpl(economia, fvp, investimento, custoCte, fvp2){
    //console.log('economia=' + economia +';fvp=' + fvp + 'investimento=' + investimento + 'custoCte=' + custoCte + 'fvp2='+ fvp2);
    return economia * fvp - investimento - custoCte * fvp2;
}

function calculaVal(fvp2){
    return 1/fvp2;;
}

function calculaRelacaoBeneficioCusto(economia, fvp, investimento, custoCte, fvp2){
    return (economia*fvp)/(investimento+custoCte*fvp2);
}

function calculaRelacaoCustoBeneficio(relacaoBeneficioCusto){
    return 1/relacaoBeneficioCusto;
}

function calcula(investimento, custoCte, juros, economia, vidaUtil, aumentoEnergia){

    var erro = parseFloat("1.0");
    var ano = parseFloat("1.0");
    var addAno = parseFloat("0.005");
    for( var i = parseFloat("1.0"); i <= vidaUtil * 5; i = i + addAno ){

        //fvp => ((((1+aumento_energia)^ano)-((1+juros)^ano))/((1+aumento_energia)-(1+juros)))*(1/((1+juros)^ano))
        var fvp = calculaFvp(i, aumentoEnergia, juros)


        //fvp=((1+juros)^ano-1)/(juros*(1+juros)^ano)
        var fvp2 = calculaFvp2(i, juros);


        //VPL ($)=economia*fvp-investimento-custo*fvp2
        var vpl = calculaVpl(economia, fvp, investimento, custoCte, fvp2);

        //var val = 1/fvp2;
        var val = calculaVal(fvp2);
        //var relacaoBeneficioCusto = (economia*v17)/(investimento+custoCte*v23);
        var relacaoBeneficioCusto = calculaRelacaoBeneficioCusto(economia, fvp, investimento, custoCte, fvp2);

        //var v61 = 1/v51;
        var relacaoCustoBeneficio = calculaRelacaoCustoBeneficio(relacaoBeneficioCusto);

        var calcErro = Math.abs(relacaoBeneficioCusto - parseFloat("1.0"));
        if( calcErro < erro ){
            erro = calcErro;
            ano = i;
        }else {
            break;
        }

        //console.log('ano=' + i + ';fvp=' + fvp + ';fvp2=' + fvp2 + ';vpl=' + vpl + ';val=' + val + ';b/c=' + relacaoBeneficioCusto + ';c/b=' + relacaoCustoBeneficio);
    }

    var fvpFinal = calculaFvp(vidaUtil, aumentoEnergia, juros)
    var fvp2Final = calculaFvp2(vidaUtil, juros);
    var vplFinal = calculaVpl(economia, fvpFinal, investimento, custoCte, fvp2Final);
    var relacaoBeneficioCustoFinal = calculaRelacaoBeneficioCusto(economia, fvpFinal, investimento, custoCte, fvp2Final);
    var relacaoCustoBeneficioFinal = calculaRelacaoCustoBeneficio(relacaoBeneficioCustoFinal);

    //console.log('vidaUtil=' + vidaUtil + ';fvp=' + fvpFinal + ';fvp2=' + fvp2Final + ';vpl=' + vplFinal + ';b/c=' + relacaoBeneficioCustoFinal + ';c/b=' + relacaoCustoBeneficioFinal);

    //console.log('ano='+ano.toFixed(2));

    return { paybackAnos : ano.toFixed(2), vpl : vplFinal.toFixed(2), relacaoCustoBeneficio : relacaoCustoBeneficioFinal.toFixed(2)};

}

function geraDadosGrafico(investimento, custoCte, juros, economia, vidaUtil, aumentoEnergia){

    var dados = [];


    for( var i = 1; i <= vidaUtil; i++ ){

        var fvp = calculaFvp(i, aumentoEnergia, juros)

        var fvp2 = calculaFvp2(i, juros);

        var vpl = calculaVpl(economia, fvp, investimento, custoCte, fvp2);

        dados.push([i, vpl]);

    }

    return dados;

}