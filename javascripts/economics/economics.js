$(document).ready(function(){
	var option = '';

	option += '<option value="A">0-10 '+getUnit()+'</option>';
	option += '<option value="B">10-20 '+getUnit()+'</option>';
	option += '<option value="C">20-50 '+getUnit()+'</option>';
	option += '<option value="D">50-100 '+getUnit()+'</option>';
	option += '<option value="E">100-200 '+getUnit()+'</option>';
	option += '<option value="F">>= 200 '+getUnit()+'</option>';

	$('#classConsumption').append(option);

	$.get("/infosan-analytics-api/meters/all", function(data, status){

		var option = '';
		for (var i=0;i<data.length;i++){
			option += '<option value="'+ data[i].id + '">' + data[i].id + ' - ' + data[i].meterModelName + '</option>';
		}
		$('#meterModel').append(option);
	
	});

});

function getUrlParameter(){

	var numOfConnections = $('#numOfConnections').val();
	var avgTax = $('#avgTax').val();
	var replacementCharge = $('#replacementCharge').val();
	var yearsOfLoss = $('#yearsOfLoss').val();
	var yearsOfInstall = $('#yearsOfInstall').val();
	var classConsumption = $('#classConsumption').val();	
	var meterModel = $('#meterModel').val();	

	return "numberOfElements={0}&avgTax={1}&replacementCharge={2}&yearsOfLoss={3}&yearsOfInstall={4}&classConsumption={5}&meterModel={6}"
			.format(numOfConnections, avgTax, replacementCharge, yearsOfLoss, yearsOfInstall, classConsumption, meterModel);

}

function filterTopChart()
{
	//"{0}{1}".format("{1}", "{0}")
	var urlParameter = getUrlParameter();
	if($('#typeAnalisys').val() == "ER")
	{
		request_url = '/infosan-analytics-api/economics/topEfficiencyReplacementByModel?' + urlParameter;
	}
	else
	{
		request_url = '/infosan-analytics-api/economics/topRecoveryByModel?' + urlParameter;
	}

	$.get(request_url , function(data, status){
	
		$('#connections_table').remove();

		if(data.length > 0)//there are itens on the list
		{

			var meter_model = [];
			var recovery_value = [];
			var consumption_losses = [];
			var number_of_connections = [];
			var replacement_cost = [];
			
			for (var idx in data) {
				meter_model.push(data[idx].meterModelName);
				recovery_value.push(data[idx].recoveryValue);
				consumption_losses.push(data[idx].consumptionLosses);
				number_of_connections.push(data[idx].numberOfConnections);
				replacement_cost.push(data[idx].replacementCost);
			}			

			//economics_top_chart
			Plotly.newPlot("economics_top_chart", [
				{
					"type": "bar", 
					"x": meter_model , 
					"y": recovery_value,
					"name": "Recovery Value",
					"marker": {"color": "#3772FF"}
				},
				{
					"type": "scatter",
					"x": meter_model ,
					"y": consumption_losses,
					"yaxis": "y2",
					"name": "Consumption Losses",
					"marker":{
						"color":"green"
					}
				},
				{
					"type": "scatter",
					"x": meter_model ,
					"y": number_of_connections,
					"yaxis": "y2",
					"name": "Number of Connections",
					"marker":{
						"color":"yellow"
					}
				},
				{
					"type": "scatter",
					"x": meter_model ,
					"y": replacement_cost,
					"yaxis": "y2",
					"name": "Replacement Cost",
					"marker":{
						"color":"red"
					}
				}],
				{
					"paper_bgcolor": "#fff",
					"plot_bgcolor": "#fff",
					"font": {
						"color": "#000"
					},
					"xaxis": {
						"title": "Models"
					},
					"yaxis": {
						"side": "right",
						"title": "Recover Value",
						"showgrid": false,
						"zeroline": false
					},
					"title": "Top Recovery and Losses per Model", 
					"yaxis2": {
						"side": "left",
						"overlaying": "y",
						"title": "Consumption Losses",
						"showgrid": false, 
						"zeroline": false
					}
				},
				{
					"showLink": false,
					"linkText": "Export to plot.ly"
				});			
		} 
		else
		{
			html_to_append = "<p id='connections_table' > No connections found ! </p>"
			$('#result-table').append(html_to_append);
		}

	});
	

}

function filter()
{
	//"{0}{1}".format("{1}", "{0}")
	var urlParameter = getUrlParameter();
	if($('#typeAnalisys').val() == "ER")
	{
		request_url = '/infosan-analytics-api/economics/topEfficiencyReplacement?' + urlParameter;
	}
	else
	{
		request_url = '/infosan-analytics-api/economics/topRecovery?' + urlParameter;
	}

	$.get(request_url , function(data, status){
	
		$('#connections_table').remove();

		if(data.length > 0)//there are itens on the list
		{

			html_to_append = 
			'<table id="connections_table" class="ls-table" >' +
				'<tr>' +
					'<th> ID </th>' +
					'<th> Model </th>' +
					'<th> Avg Consumption </th>' +
					'<th> Losses </th>' +
					'<th> Replacement Cost </th>' +
					'<th> Consumption Interval </th>' +
				'</tr>' +
			'</table>';

			$('#result-table').append(html_to_append);

			for(var i = 0 ; i < data.length;i++)
			{
				economic = data[i];

				html_to_append = 
				'<tr>' +
					'<td>' + economic.connectionId + '</td>' +
					'<td>' + economic.meterModelName + '</td>' +
					'<td>' + economic.consumptionAvg + '</td>' +
					'<td>' + economic.consumptionLosses + '</td>' +
					'<td>' + economic.replacementCost + '</td>' +
					'<td>' + economic.interval + '</td>' +
				'</tr>';

				$('#connections_table').append(html_to_append);
			}
		} 
		else
		{
			html_to_append = "<p id='connections_table' > No connections found ! </p>"
			$('#result-table').append(html_to_append);
		}

	});
	

}