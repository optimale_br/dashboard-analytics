$.get("/infosan-analytics-api/meters/all", function(data, status){

    var option = '';
    for (var i=0;i<data.length;i++){
        option += '<option value="'+ data[i].id + '">' + data[i].id + ' - ' + data[i].meterModelName + '</option>';
    }
    $('#meterModel').append(option);

});

function btnFilterClick(){

    if($( "#meterModel" ).val()){
        $.get("/infosan-analytics-api/losses/consumption-by-class/" + $( "#meterModel" ).val(), consumptionByClass);
    }else{
        alert('Select a model');
    }

}



//$.get("/infosan-analytics-api/losses/consumption-by-class/MC150", consumptionByClass); 

function consumptionByClass(data, status){

	var class_consumption = [];
	var consumption_cumulative_sum = [];
    var consumption_losses = [];    
    var ratio_losses = [];
    
    for (var idx in data) {
	    class_consumption.push(data[idx].interval);
	    consumption_cumulative_sum.push(data[idx].consumptionCumulativeSum + data[idx].consumptionLosses);
        consumption_losses.push(data[idx].consumptionLosses);
        ratio_losses.push(data[idx].ratioLosses * 100);
    }

	//console.log(data);

    var trace1 = {
        x: class_consumption,
        y: consumption_losses,
        name: 'Losses',
        type: 'bar'
    };
    
    var trace2 = {
        x: class_consumption,
        y: consumption_cumulative_sum,
        name: 'Total Consumption',
        type: 'bar'
    };

    var trace3 = {
        x: class_consumption,
        y: ratio_losses,
        yaxis: 'y2',
        name: 'Loss Ratio (%)',
        mode: 'lines',
        type: 'scatter'
    };
      
    var data = [trace1, trace2, trace3];
      
    var layout = {barmode: 'stack'};
      
    Plotly.newPlot('consumption_by_class', data, 
    {
        "paper_bgcolor": "#fff",
        "plot_bgcolor": "#fff",
        "width": "1100",
        "height": "500",
        "legend": {
            "orientation": "h",
            "x": "0",
            "y": "500"
        }, 
        "font": {
            "color": "#000"
        },
        "xaxis": {
            "title": "Intervals"
        },
        "yaxis": {
            "side": "left",
            "title": "Volume ("+getUnit()+")",
            "showgrid": false,
            "zeroline": false
        },
        "title": "Consumption Losses per Model " + $( "#meterModel option:selected" ).text(), 
        "yaxis2": {
            "side": "right",
            "overlaying": "y",
            "title": "Loss Ratio (%)",
            "showgrid": false, 
            "zeroline": false
        }
    });



}

$.get("/infosan-analytics-api/losses/top-ratio-losses", function(data, status){

	var meter_model = [];
	var consumption_cumulative_sum = [];
    var consumption_losses = [];    
    var ratio_losses = [];
    
    for (var idx in data) {
	    meter_model.push(data[idx].meterModel);
	    consumption_cumulative_sum.push(data[idx].consumptionCumulativeSum + data[idx].consumptionLosses);
        consumption_losses.push(data[idx].consumptionLosses);
        ratio_losses.push(data[idx].ratioLosses * 100);
    }

	Plotly.newPlot("top_ratio_losses", [
        {
            "type": "bar", 
            "x": meter_model , 
            "y": ratio_losses,
            "name": "Loss Ratio (%)",
            "marker": {"color": "#254fa2"}
        },
        {
            "type": "scatter",
            "x": meter_model ,
            "y": consumption_cumulative_sum,
            "yaxis": "y2",
            "name": "Consumption",
            "marker":{
                "color":"green"
            }
        }],
        {
            "paper_bgcolor": "#fff",
            "plot_bgcolor": "#fff",
            "width": "1100",
            "height": "500",
            "legend": {
                "orientation": "v",
                "x": "0",
                "y": "500"
            },
            "font": {
                "color": "#000"
            },
            "xaxis": {
                "title": "Models"
            },
            "yaxis": {
                "side": "right",
                "title": "Ratio(%)",
                "showgrid": false,
                "zeroline": false
            },
            "title": "Top 50 Ratio Losses per Model", 
            "yaxis2": {
                "side": "left",
                "overlaying": "y",
                "title": "Volume ("+getUnit()+")",
                "showgrid": false, 
                "zeroline": false
            }
        },
        {
            "showLink": false,
            "linkText": "Export to plot.ly"
        });

});

$.get("/infosan-analytics-api/losses/top-class", function(data, status){

	var classes = [];
	var consumption_cumulative_sum = [];
    var consumption_losses = [];    
    var ratio_losses = [];
    
    for (var idx in data) {
	    classes.push(data[idx].interval);
	    consumption_cumulative_sum.push(data[idx].consumptionCumulativeSum + data[idx].consumptionLosses);
        consumption_losses.push(data[idx].consumptionLosses);
        ratio_losses.push(data[idx].ratioLosses * 100);
    }

	Plotly.newPlot("top_class", [
        {
            "type": "bar", 
            "x": classes , 
            "y": ratio_losses,
            "name": "Interval",
            "marker": {"color": "#254fa2"}
        },
        {
            "type": "scatter",
            "x": classes ,
            "y": consumption_cumulative_sum,
            "yaxis": "y2",
            "name": "Consumption",
            "marker":{
                "color":"green"
            }
        }],
        {
            "paper_bgcolor": "#fff",
            "plot_bgcolor": "#fff",
            "width": "1100",
            "height": "500",
            "legend": {
                "orientation": "h",
                "x": "0",
                "y": "500"
            },
            "font": {
                "color": "#000"
            },
            "xaxis": {
                "title": "Intervals"
            },
            "yaxis": {
                "side": "right",
                "title": "Lost Ratio (%)",
                "showgrid": false,
                "zeroline": false
            },
            "title": "Ratio Losses per Interval", 
            "yaxis2": {
                "side": "left",
                "overlaying": "y",
                "title": "Volume ("+getUnit()+")",
                "showgrid": false, 
                "zeroline": false
            }
        },
        {
            "showLink": false,
            "linkText": "Export to plot.ly"
        });

});
