$.ajaxSetup({
    beforeSend: function (xhr) {
        xhr.setRequestHeader('X-TenantID',  window.sessionStorage.getItem('X-TenantID'));
    },       
});

if (!String.prototype.format) {
    String.prototype.format = function() {
        var formatted = this;
        for (var i = 0; i < arguments.length; i++) {
            var regexp = new RegExp('\\{'+i+'\\}', 'gi');
            formatted = formatted.replace(regexp, arguments[i]);
        }
        return formatted;
    }
};

function eraseCookie(name) {   
    window.sessionStorage.removeItem('X-TenantID');
    window.sessionStorage.removeItem('user');
    window.location.href = "../login.html";
}

function verificaLogin(){
    var x = getUser();
    if (!x) {
        window.location.href = "../login.html";
    }
}

function getTenantID(){
    return window.sessionStorage.getItem('X-TenantID');
}

function getUser(){
    var strUser = window.sessionStorage.getItem('user');
    if(strUser){
        var user = JSON.parse(strUser);
        return user;
    }else{
        return '';
    }
}

function getUnit(){
    return getUser().unit;
}

verificaLogin();

$(document).ready(function(){
    if(getUser() && getUser().profile == 'only-consumption'){
        $( ".meter-data" ).hide();
    }
});    