$(document).ready(function(){
    $.get("/infosan-analytics-api/meters/all", function(data, status){

        var option = '';
        for (var i=0;i<data.length;i++){
            option += '<option value="'+ data[i].id + '">' + data[i].id + ' - ' + data[i].meterModelName + '</option>';
        }
        $('#meterModel').append(option);

    });
});
function btnFilterClick(){

    if($( "#meterModel" ).val()){
        $.get("/infosan-analytics-api/views/vw_consumption_cumulative_by_class?meter_model=" + $( "#meterModel" ).val(), vw_consumption_cumulative_by_class);
    }else{
        alert('Select a model');
    }

}

// vw_amount_of_meters_and_total_consumption
$.get("/infosan-analytics-api/views/vw_amount_of_meters_and_total_consumption", function(data, status){

    var intervals = [];
    var amount_of_meters = [];
    var sum = [];

    for (var idx in data) {
        intervals.push(data[idx].consumptionDate)
        amount_of_meters.push(data[idx].count)
        sum.push(data[idx].sum)
    }   



    Plotly.newPlot("vw_amount_of_meters_and_total_consumption", 
        [
          {"type": "bar", 
            "x": intervals,
            "y": amount_of_meters,
            "marker" : {"color" : "green"},
            "name" : "Amount of meters"
          },
          {"type": "scatter", 
            "x": intervals,
            "y": sum,
            "yaxis" : "y2",
            "name" : "Consumption",
            "marker" : {"color" : "#254fa2"}
          }
        ], 
        {
            "paper_bgcolor": "#fff", 
            "plot_bgcolor": "#fff", 
            "width": "1100",
            "height": "500",
            "legend": {
                "orientation": "h",
                "x": "0",
                "y": "1"
                
            },
            
            "font": {"color": "#000"}, 
            "xaxis": {"title": "Amount of meters per month and the evolution of the volume"}, 
            "yaxis": {
            "side": "left",
            "title": "Amount of meters",
            "showgrid": false,
            "zeroline": false
        },
        "yaxis2": {
            "side": "right",
            "overlaying": "y",
            "title": "Volume ("+getUnit()+")",
            "showgrid": false, 
            "zeroline": false
        },
         "title": "Intervals of consumptions"},
         {"showLink": false, "linkText": "Export to plot.ly"})

});


// vw_consumptions_interval
$.get("/infosan-analytics-api/views/vw_intervals_of_consumption", function(data, status){

    var intervals = [];
    var amount_of_connections = [];

    for(var idx in data)
    {
        intervals.push(data[idx].interval)
        amount_of_connections.push(data[idx].amount)
    }

	Plotly.newPlot("vw_intervals_of_consumption", 
		[
		  {"type": "bar", 
		    "x": intervals,
		    "y": amount_of_connections,
		    "marker" : {"color" : "#254fa2"}
		  }
		], 
	    {
            "paper_bgcolor": "#fff", 
         "plot_bgcolor": "#fff",
         "width": "1100",
            "height": "500", 
	     "font": {"color": "#000"}, 
	     "xaxis": {"title": "Intervals ("+getUnit()+")"}, 
	     "yaxis": {"title": "Amount of connections"}, 
	     "title": "Intervals of consumptions"},
	     {"showLink": false, "linkText": "Export to plot.ly"})

});

$.get("/infosan-analytics-api/views/vw_consumption_cumulative_by_class?meter_model=MC002", vw_consumption_cumulative_by_class);

function vw_consumption_cumulative_by_class(data, status){

    var intervals = [];
    var amount_of_connections = [];

    for(var idx in data)
    {
        intervals.push(data[idx].classConsumption)
        amount_of_connections.push(data[idx].count)
    }

    Plotly.newPlot("vw_consumption_cumulative_by_class", 
        [
          {"type": "bar", 
            "x": intervals,
            "y": amount_of_connections,
            "marker" : {"color" : "#254fa2"}
          }
        ], 
        {
            "paper_bgcolor": "#fff", 
            "plot_bgcolor": "#fff",
            "width": "1100",
            "height": "500",
            "legend": {
                "orientation": "h",
                "x": "0",
                "y": "500"
            }, 
            "font": {"color": "#000"}, 
            "xaxis": {"title": "Intervals"}, 
            "yaxis": {"title": "Percentual of connections in the range"}, 
            "title": "Intervals of consumptions - " + $( "#meterModel option:selected" ).text()},
            {"showLink": false, "linkText": "Export to plot.ly"})

}

$.get("/infosan-analytics-api/views/vw_consumptions_average_and_trend", function(data, status){

    var dates = [];
    var averages = [];
    var amount_of_meters = [];
    var consumption_trend = [];

    for(var idx in data)
    {
        dates.push(data[idx].consumptionDate)
        averages.push(data[idx].consumptionAverage)
        amount_of_meters.push(data[idx].amountOfMeters)
        consumption_trend.push(data[idx].consumption_trend)
    }

	// vw_consumptions_average_and_trend plot
	Plotly.newPlot("vw_consumptions_average_and_trend", [
    {
        "type": "bar", 
        "x": dates , 
        "y": amount_of_meters,
        "name": "Meters",
        "marker": {"color": "#254fa2"}
    },
    {
        "type": "scatter",
        "x": dates ,
        "y":averages ,
        "yaxis": "y2",
        "name": "Volume ("+getUnit()+")",
        "marker":{
            "color":"green"
        }
    },
    {
        "type": "scatter",
        "x": dates ,
        "y":  consumption_trend, 
        "name": "trend",
        "mode": "lines",
        "marker": {
            "color": "#F038FF"
        },
        "yaxis": "y3"
    }],
    {
        
        "paper_bgcolor": "#fff",
        "plot_bgcolor": "#fff",
        "width": "1100",
        "height": "500",
        "legend": {
            "orientation": "h",
            "x": "0",
            "y": "500"
        },
       
        "font": {
            "color": "#000"
        },
        "xaxis": {
            "title": "Month"
        },
        "yaxis": {
            "side": "right",
            "title": "Meters",
            "showgrid": false,
            "zeroline": false
		    
        },
        "title": "Evolution of the monthly measured volumes and the amount of installed meters", 
        "yaxis2": {
            "side": "left",
            "overlaying": "y",
            "title": "Volume ("+getUnit()+")",
            "showgrid": false, 
            "zeroline": false
        },
        "yaxis3": {
            "overlaying": "y",
            "showgrid": false,
            "zeroline": false,
            "autorange": true,
            "showline": false,
            "autotick": true,
            "ticks": "",
            "showticklabels": false
        }
    },
    {
        "showLink": false,
        "linkText": "Export to plot.ly"
        
        
       
    });



});