$(document).ready(function(){
	var option = '';

	option += '<option value="A">0-10 '+getUnit()+'</option>';
	option += '<option value="B">10-20 '+getUnit()+'</option>';
	option += '<option value="C">20-50 '+getUnit()+'</option>';
	option += '<option value="D">50-100 '+getUnit()+'</option>';
	option += '<option value="E">100-200 '+getUnit()+'</option>';
	option += '<option value="F">>= 200 '+getUnit()+'</option>';

	$('#interval').append(option);

	$.get("/infosan-analytics-api/meters/all", function(data, status){

		var option = '';
		for (var i=0;i<data.length;i++){
			option += '<option value="'+ data[i].id + '">' + data[i].id + ' - ' + data[i].meterModelName + '</option>';
		}
		$('#model').append(option);
		
	});


});

function generateConsumptionPlot(connection_id){

	request_url = "/infosan-analytics-api/consumption/find?connection_id=" + connection_id; 
	$.get(request_url , function(data, status){

			intervals = data['consumptionDate'];
			consumptions = data['consumptionMonth'];

			modal_title = "The consumptions and trend of the connection : " + connection_id;
			//$('#modal-title').html(modal_title);

			$('#plot-modal').modal('toggle');

			console.log(data);
			Plotly.newPlot("connection_consumptions_plot", 
	        [
	          {"type": "scatter", 
	            "x": intervals,
	            "y": consumptions,
	            "name" : "Consumption",
	            "marker" : {"color" : "#254fa2"}
	          }
	        ], 
	        {
	            "paper_bgcolor": "#fff", 
	            "plot_bgcolor": "#fff", 
	            "width": "500",
	            "height": "500",
	            "legend": {
	                "orientation": "h",
	                "x": "0",
	                "y": "1"
	                
	            },
	            
	            "font": {"color": "#000"}, 
	            "xaxis": {"title": "Month"}, 
	            "yaxis": {
	            "side": "left",
	            "title": "Consumption average - "+getUnit(),
	            "showgrid": false,
	            "zeroline": false
	        },
	         "title": modal_title},
	         {"showLink": false, "linkText": "Export to plot.ly"});



		});


}


function onButtonClick()
{
	interval =  $('#trends-form').find('#interval').val();
	model = $('#trends-form').find('#model').val();
	request_url = "/infosan-analytics-api/consumption_cumulative_trends"

	if(!interval || !model)
	{
		alert("You must insert the consumption interval and the meter model !");
	}
	else
	{
		request_url = request_url + '?interval=' + interval + '&model=' + model;

		$.get(request_url , function(data, status){
		
			$('#connections_table').remove();

			if(data.length > 0)//there are itens on the list
			{

				html_to_append = 
				'<table id="connections_table" class="ls-table" >' +
					'<tr>' +
						'<th> ID </th>' +
						'<th> Consumption average </th>' +
						'<th> Cumulated consumption </th>' +
						'<th> Diameter </th>' +
						'<th> Meter install date </th>' +
						'<th> Meter model </th>' +
						'<th> Slope </th>' +
					'</tr>' +
				  '</table>';

				$('#result-table').append(html_to_append);

				for(var i = 0 ; i < data.length;i++)
				{
					cumulative_consumption = data[i];


					id = cumulative_consumption['connectionId'];
					average = cumulative_consumption['consumptionAvg'];
					cumulative = cumulative_consumption['consumptionCumulative'];
					diameter = cumulative_consumption['meterDn'];
					meter_install_date = new Date(cumulative_consumption['meterInstallDate']);
					meter_model = cumulative_consumption['meterModel'];
					slope = cumulative_consumption['slope'];

					html_to_append = 
					'<tr onclick="generateConsumptionPlot(' + id  + ');" class="connection-item" >' +
						'<td class="connection-id">' + id + '</td>' +
						'<td>' + average + '</td>' +
						'<td>' + cumulative + '</td>' +
						'<td>' + diameter + '</td>' +
						'<td>' + meter_install_date.toDateString() + '</td>' +
						'<td>' + meter_model + '</td>' +
						'<td>' + slope + '</td>'+
					'</tr>';

					$('#connections_table').append(html_to_append);
				}
			} 
			else
			{
				html_to_append = "<p id='connections_table' > No connections found ! </p>"
				$('#result-table').append(html_to_append);
			}

		});
	}
}

function getUrlParameter(){

	var numberOfElements = $('#numberOfElements').val();	
	var minNumberOfMonths = $('#minNumberOfMonths').val();	
	var classConsumption = $('#interval').val();	
	var meterModel = $('#model').val();	

	return "numberOfElements={0}&minNumberOfMonths={1}&classConsumption={2}&meterModel={3}"
			.format(numberOfElements, minNumberOfMonths, classConsumption, meterModel);

}

function filterTopChart()
{

	var urlParameter = getUrlParameter();
	request_url = '/infosan-analytics-api/trends/topByModel?' + urlParameter;

	$.get(request_url , function(data, status){
	
		if(data.length > 0)//there are itens on the list
		{

			var meter_model = [];
			var number_of_connections = [];
			var total_of_connections = [];
			var percentage = [];
			
			for (var idx in data) {
				meter_model.push(data[idx].meterModelName);
				number_of_connections.push(data[idx].numberOfConnections);
				total_of_connections.push(data[idx].totalOfConnections);
				percentage.push(data[idx].percentage);
			}			

			//economics_top_chart
			Plotly.newPlot("trends_top_chart", [
				{
					"type": "bar", 
					"x": meter_model , 
					"y": number_of_connections,
					"name": "Number of Connections",
					"marker": {"color": "#3772FF"}
				},
				{
					"type": "bar", 
					"x": meter_model , 
					"y": total_of_connections,
					"name": "Total of Connections",
					"marker": {"color": "green"}
				},
				{
					"type": "scatter",
					"x": meter_model ,
					"y": percentage,
					"yaxis": "y2",
					"name": "Percentage",
					"marker":{
						"color":"red"
					}
				}],
				{
					"paper_bgcolor": "#fff",
					"plot_bgcolor": "#fff",
					"font": {
						"color": "#000"
					},
					"xaxis": {
						"title": "Models"
					},
					"yaxis": {
						"side": "right",
						"title": "Number of Connections",
						"showgrid": false,
						"zeroline": false
					},
					"yaxis2": {
						"side": "left",
						"overlaying": "y",
						"title": "Percentage",
						"showgrid": false, 
						"zeroline": false
					}					
				},
				{
					"showLink": false,
					"linkText": "Export to plot.ly"
				});			
		} 
		else
		{
			html_to_append = "<p id='connections_table' > No connections found ! </p>"
			$('#filterTopChart').append(html_to_append);
		}

	});
	

}