


function onButtonClick(kind)
{
	if(kind == 'higher_less')
	{
		form = $('#higher_less');
		request_url = '/infosan-analytics-api/consumption_cumulative_consumption_less_than_diameter_higher_than?'
	}
	else
	{
		form = $('#less_higher');
		request_url = '/infosan-analytics-api/consumption_cumulative_consumption_higher_than_diameter_less_than?'
	}


	consumption_average =  form.find('.consumption_average').val();
	meter_diameter = form.find('.meter_diameter').val();

	if(!consumption_average || !meter_diameter)
	{
		alert("You must insert the consumption average and the meter diameter !");
	}
	else
	{
		request_url = request_url + 'consumption_avg=' + consumption_average + '&diameter=' + meter_diameter;

		$.get(request_url , function(data, status){
		
			$('#connections_table').remove();

			if(data.length > 0)//there are itens on the list
			{

				html_to_append = 
				'<table id="connections_table" class="ls-table" >' +
					'<tr>' +
						'<th> ID </th>' +
						'<th> Consumption average </th>' +
						'<th> Cumulated consumption </th>' +
						'<th> Diameter </th>' +
						'<th> Meter install date </th>' +
						'<th> Meter model </th>' +
					'</tr>' +
				  '</table>';

				$('#result-table').append(html_to_append);

				for(var i = 0 ; i < data.length;i++)
				{
					cumulative_consumption = data[i];


					id = cumulative_consumption['connectionId'];
					average = cumulative_consumption['consumptionAvg'];
					cumulative = cumulative_consumption['consumptionCumulative'];
					diameter = cumulative_consumption['meterDn'];
					meter_install_date = new Date(cumulative_consumption['meterInstallDate']);
					meter_model = cumulative_consumption['meterModel'];

					html_to_append = 
					'<tr>' +
						'<td>' + id + '</td>' +
						'<td>' + average + '</td>' +
						'<td>' + cumulative + '</td>' +
						'<td>' + diameter + '</td>' +
						'<td>' + meter_install_date.toDateString() + '</td>' +
						'<td>' + meter_model + '</td>' +
					'</tr>';

					$('#connections_table').append(html_to_append);
				}
			} 
			else
			{
				html_to_append = "<p id='connections_table' > No connections found ! </p>"
				$('#result-table').append(html_to_append);
			}

		});
	}

	 
	

}