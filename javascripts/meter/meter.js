$.get("/infosan-analytics-api/meters/all", function(data, status){

    var option = '';
    for (var i=0;i<data.length;i++){
       option += '<option value="'+ data[i].id + '">' + data[i].id + ' - ' + data[i].meterModelName + '</option>';
    }
    $('#meterModel').append(option);

});

/*$("#btnFilter").click( function()
{
  alert('button clicked');
}
);*/

function btnFilterClick(){

    if($( "#meterModel" ).val()){
        $.get("/infosan-analytics-api/meter/install-consumption-by-model/" + $( "#meterModel" ).val(), installConsumptionByModel);
    }else{
        alert('Select a model');
    }

}

function topInstall(data, status){

	var models = [];
	var amount_of_consumption = [];
    var amount_of_meters = [];    
    
    for (var idx in data) {
        models.push(data[idx].modelName);
        amount_of_consumption.push(data[idx].totalConsumption);
        amount_of_meters.push(data[idx].totalInstall);
    }

	//console.log(models);

	Plotly.newPlot("top_install", [
    {
        "type": "bar", 
        "x": models , 
        "y": amount_of_meters,
        "name": "Install",
        "marker": {"color": "#254fa2"}
    },
    {
        "type": "scatter",
        "x": models ,
        "y":amount_of_consumption ,
        "yaxis": "y2",
        "name": "Consumption",
        "marker":{
            "color":"green"
        }
    }],
    {
        "paper_bgcolor": "#fff",
        "plot_bgcolor": "#fff",
        "width": "1100",
        "height": "500",
        "legend": {
            "orientation": "h",
            "x": "0",
            "y": "500"
        }, 
        "font": {
            "color": "#000"
        },
        "xaxis": {
            "title": "Models"
        },
        "yaxis": {
            "side": "right",
            "title": "Meters",
            "showgrid": false,
            "zeroline": false
        },
        "title": "Top 20 Install Meters vs Consumption per Model", 
        "yaxis2": {
            "side": "left",
            "overlaying": "y",
            "title": "Volume",
            "showgrid": false, 
            "zeroline": false
        }
    },
    {
        "showLink": false,
        "linkText": "Export to plot.ly"
    });

}

$.get("/infosan-analytics-api/meter/top-install", topInstall);

$.get("/infosan-analytics-api/meter/top-consumption", function(data, status){

	var models = [];
	var amount_of_consumption = [];
    var amount_of_meters = [];    
    
    for (var idx in data) {
        models.push(data[idx].modelName);
        amount_of_consumption.push(data[idx].totalConsumption);
        amount_of_meters.push(data[idx].totalInstall);
    }

	//console.log(models);

	Plotly.newPlot("top_consumption", [
    {
        "type": "bar", 
        "x": models , 
        "y": amount_of_consumption,
        "name": "Consumption",
        "marker": {"color": "#254fa2"}
    },
    {
        "type": "scatter",
        "x": models ,
        "y": amount_of_meters,
        "yaxis": "y2",
        "name": "Install",
        "marker":{
            "color":"green"
        }
    }],
    {
        "paper_bgcolor": "#fff",
        "plot_bgcolor": "#fff",
        "width": "1100",
        "height": "500",
        "legend": {
            "orientation": "h",
            "x": "0",
            "y": "500"
        }, 
        "font": {
            "color": "#000"
        },
        "xaxis": {
            "title": "Models"
        },
        "yaxis": {
            "side": "right",
            "title": "Meters",
            "showgrid": false,
            "zeroline": false
        },
        "title": "Top 20 Consumption vs Install Meters per Model", 
        "yaxis2": {
            "side": "left",
            "overlaying": "y",
            "title": "Volume",
            "showgrid": false, 
            "zeroline": false
        }
    },
    {
        "showLink": false,
        "linkText": "Export to plot.ly"
    });

});


//$.get("/infosan-analytics-api/meter/install-consumption-by-model/MC150", installConsumptionByModel);

function installConsumptionByModel(data, status){

	var dates = [];
	var amount_of_consumption = [];
    var amount_of_meters = [];    
    var amount_of_global_consumption = [];
    var amount_of_global_install = [];
    
    for (var idx in data) {
        dates.push(data[idx].date);
        amount_of_consumption.push(data[idx].totalConsumption);
        amount_of_meters.push(data[idx].totalInstall);
        amount_of_global_consumption.push(data[idx].allConsumption);
        amount_of_global_install.push(data[idx].allInstall);
    }

	//console.log(data);

	Plotly.newPlot("install_consumption_by_model-consumption", [
    {
        "type": "bar", 
        "x": dates , 
        "y": amount_of_meters,
        "name": "Install",
        "marker": {"color": "#254fa2"}
    },
    {
        "type": "scatter",
        "x": dates ,
        "y": amount_of_consumption,
        "yaxis": "y2",
        "name": "Consumption",
        "marker":{
            "color":"green"
        }
    },
    {
        "type": "scatter",
        "x": dates ,
        "y":  amount_of_global_consumption, 
        "name": "Global Consumption",
        "mode": "lines",
        "marker": {
            "color": "#F038FF"
        },
        "yaxis": "y3"
    }],
    {
        "paper_bgcolor": "#fff",
        "plot_bgcolor": "#fff",
        "width": "1100",
        "height": "500",
        "legend": {
            "orientation": "h",
            "x": "0",
            "y": "500"
        }, 
        "font": {
            "color": "#000"
        },
        "xaxis": {
            "title": "Months"
        },
        "yaxis": {
            "side": "right",
            "title": "Install Meters",
            "showgrid": false,
            "zeroline": false
        },
        "title": "Global Consumption vs Consumption vs Install Meters per Model " + $( "#meterModel option:selected" ).text(), 
        "yaxis2": {
            "side": "left",
            "overlaying": "y",
            "title": "Consumption",
            "showgrid": false, 
            "zeroline": false
        },
        "yaxis3": {
            "overlaying": "y",
            "showgrid": false,
            "zeroline": false,
            "autorange": true,
            "showline": false,
            "autotick": true,
            "ticks": "",
            "showticklabels": false
        }
    },
    {
        "showLink": false,
        "linkText": "Export to plot.ly"
    });

	Plotly.newPlot("install_consumption_by_model-install", [
        {
            "type": "bar", 
            "x": dates , 
            "y": amount_of_consumption,
            "name": "Consumption",
            "marker": {"color": "#254fa2"}
        },
        {
            "type": "scatter",
            "x": dates ,
            "y": amount_of_meters,
            "yaxis": "y2",
            "name": "Install",
            "marker":{
                "color":"green"
            }
        },
        {
            "type": "scatter",
            "x": dates ,
            "y":  amount_of_global_install, 
            "name": "Global Install",
            "mode": "lines",
            "marker": {
                "color": "#F038FF"
            },
            "yaxis": "y3"
        }],
        {
            "paper_bgcolor": "#fff",
            "plot_bgcolor": "#fff",
            "width": "1100",
            "height": "500",
            "legend": {
                "orientation": "h",
                "x": "0",
                "y": "500"
            }, 
            "font": {
                "color": "#000"
            },
            "xaxis": {
                "title": "Months"
            },
            "yaxis": {
                "side": "right",
                "title": "Consumption",
                "showgrid": false,
                "zeroline": false
            },
            "title": "Global Install vs Consumption vs Install Meters per Model " + $( "#meterModel option:selected" ).text(), 
            "yaxis2": {
                "side": "left",
                "overlaying": "y",
                "title": "Install",
                "showgrid": false, 
                "zeroline": false
            },
            "yaxis3": {
                "overlaying": "y",
                "showgrid": false,
                "zeroline": false,
                "autorange": true,
                "showline": false,
                "autotick": true,
                "ticks": "",
                "showticklabels": false
            }
        },
        {
            "showLink": false,
            "linkText": "Export to plot.ly"
        });    

}